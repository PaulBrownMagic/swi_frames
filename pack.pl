name(swi_frames).
version('0.0.3').
title('Frame based storage, mutable and immutable').
author('Paul Brown', 'http://paulbrownmagic.com/foaf.rdf#me').
home('https://gitlab.com/PaulBrownMagic/SWI_Frames').
