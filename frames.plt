setup_frames :-
    assert(frames:frame(thing, [inherited-thing_value])),
    assert(frames:mframe(name, [ako-thing, slotname1-value1, slotname2-value2])).

destroy_frames :-
    retractall(frames:mframe(name, _)),
    retractall(frames:frame(thing,_)).

:- begin_tests(getting_data).

:- use_module(frames).

test(get_thing_by_name_all_slots,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    get_frame(name, Slots),
    Slots = [ako-thing, slotname1-value1, slotname2-value2].

test(get_nonexistant,
    [fail
    ]) :-
    get_frame(nonexistant, _).

test(get_things_ako,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    get_frame(name, ako-AKO),
    AKO = thing.

test(get_things_any_single_property,
    [ setup(setup_frames)
    , all(P-V == [ako-thing, slotname1-value1, slotname2-value2, inherited-thing_value])
    , cleanup(destroy_frames)
    ]) :-
    get_frame(name, P-V).

test(get_things_any_list_properties,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    get_frame(name, [ako-Ako, slotname1-S1]),
    Ako = thing,
    S1 = value1.

test(get_no_slots,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    get_frame(name, []).

test(get_from_slots,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    frames:mframe(name, Slots),
    frames:get_from_slots(slotname1-value1, Slots).

test(fail_on_get_no_def,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , fail
    ]) :-
    get_frame(name, nodefinedattr-_).

test(get_frame_inherited_via_ako,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]) :-
    get_frame(name, inherited-thing_value).

test(get_wrong_val,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , fail
    ]) :-
    get_frame(name, slotname1-no_right).

test(multiple_inheritence_ako,
    [ setup(assert(frames:frame(name, [ako-[thing1, thing2]])))
    , cleanup(destroy_frames)
    , all(Ako = [thing1, thing2])
    ]) :-
    get_frame(name, ako-Ako).

test(instance_isa,
    [ setup(setup_frames)
    , cleanup(destroy_frames)
    , nondet
    ]
    ) :-
    assert(frames:frame(instance, [isa-name])),
    get_frame(instance, [ako-thing, slotname1-value1]),
    retractall(frames:frame(instance, _)).

:- end_tests(getting_data).


:- begin_tests(manipulating_data).

:- use_module(frames).

:- make_directory_path(testing).

setup_frames :-
    set_db_dir(testing),
    set_static_kb_dir(testing),
    load_static_kb(upper),
    load_mutable_kb(testdb).

cleanup_frames :-
    true.

test(setup_db_dir) :-
    set_db_dir(testing),
    frames:db_dir(testing).

test(setup_static_kb_dir) :-
    set_static_kb_dir(testing),
    frames:static_kb_dir(testing).

test(load_static_kb,
    []) :-
    load_static_kb(upper),
    current_module(upper).

test(load_dynamic,
    []) :-
    load_mutable_kb(testdb).

test(upperframe_from_static,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    ]) :-
    upper:frame(thing, _).

test(add_new_property_is_det,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    ]) :-
        add_frame(name, [ako-thing, slotname1-value1, slotname2-value2]).

test(add_new_property_is_asserted,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    , nondet
    ]) :-
    add_frame(name, newslot-newval),
    get_frame(name, newslot-newval).

test(add_new_frame_no_data,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    , nondet
    ]) :-
    add_frame(newframe, []),
    get_frame(newframe, []).

test(add_new_frame_with_data,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    , nondet
    ]) :-
    add_frame(newerframe, [attr1-v1, attr2-v2]),
    get_frame(newerframe, [attr1-v1, attr2-v2]).

test(add_multiple_new_data,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    ]) :-
    get_frame(name, S1),
    length(S1, L1),
    add_frame(name, [a1-v1, a2-v2]),
    get_frame(name, S2),
    member(a1-v1, S2), !, member(a2-v2, S2), !,
    NewLength is L1 + 2,
    length(S2, NewLength).

test(update_data,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    , nondet
    ]) :-
    frames:update_data(name, [s1-v1], [s2-v2]),
    get_frame(name, [s2-v2]).

test(remove_a_property,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    , nondet
    ]) :-
    remove_frame(name, slotname1-value1),
    get_frame(name, Slots), \+ member(slotname1-value1, Slots).

test(remove_whole_frame,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    ]) :-
    remove_frame(name),
    \+ frames:mframe(name, _).

test(remove_multiple_data,
    [ setup(setup_frames)
    , cleanup(cleanup_frames)
    ]) :-
    add_frame(name, [a1-v1, a2-v2, a3-v3]),
    remove_frame(name, [a1-v1, a2-v2]),
    \+ get_frame(name, a1-v1),
    \+ get_frame(name, a2-v2),
    get_frame(name, a3-v3), !.

:- end_tests(manipulating_data).
