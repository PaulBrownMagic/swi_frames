:- module(frames,
    [ get_frame/2
    , get_frame_slots/2
    , add_frame/2
    , remove_frame/2
    , remove_frame/1
    , dynamic_load_mutable_kb/0
    , load_mutable_kb/1
    , load_static_kb/1
    , set_db_dir/1
    , set_static_kb_dir/1
    , new_mutable_kb/1
    , change_mutable_kb/1
    , existing_kbs/1
    ]
).
/** <module> Frames

This is a query engine to a frame-based knowledge base.
It allows the knowledge based to be queried, including
get, add and remove operations.

@author Paul Brown
@tbd Frames module shouldn't have the location of the knowledge
bases hard-coded, need to make loading the upper ontologies into
a predicate and pass the directory path into load_mutable_kb as well as
loading upper ontologies.

*/

% Use persistency library for persisting user ontologies
:- use_module(library(persistency)).

% Local frame/2, declared as persistent and persisted in attached file
:- persistent(mframe(name:any, slots:list(any))).

set_db_dir(Dir) :-
    assert(db_dir(Dir)).

set_static_kb_dir(Dir) :-
    assert(static_kb_dir(Dir)).

% If kb mounted, unmount. Assert Name does not exist. Mount Name.
new_mutable_kb(Name) :-
    ground(Name),
    \+ kb_exists(Name),
    db_detach,
    load_mutable_kb(Name).

% If kb mounted, unmount. Assert Name does exist. Mount Name.
change_mutable_kb(Name) :-
    ground(Name),
    kb_exists(Name),
    db_detach,
    load_mutable_kb(Name).

kb_exists(Name) :-
    db_dir(Dir),
    directory_file_path(Dir, Name, Path),
    exists_file(Path).


existing_kbs(Names) :-
    db_dir(Dir),
    directory_files(Dir, Files),
    exclude(os_file, Files, Names).

os_file(File) :-
    member(File, ['.', ..]).

%! dynamic_load_mutable_kb is semidet.
%  Ask user for database file to work with.
dynamic_load_mutable_kb :-
   writeln("Case to explore?"),
   read(Ans),
   load_mutable_kb(Ans).

%! load_mutable_kb(+File:atom) is semidet.
%  load the database file for consulting.
load_mutable_kb(File) :-
    db_dir(Dir),  % @tbd Raise error if not found
    directory_file_path(Dir, File, Path),
    db_attach(Path, []).

load_static_kb(File) :-
    static_kb_dir(Dir),
    directory_file_path(Dir, File, Path),
    consult(Path).
    %use_module(Path, [frame/2 as upperframe]).

%! get_frame(+Name, ?Slots) is semidet.
%  Query the data to find the slots
%
%  @arg Name A unique identifier of a frame
%  @arg Slots A list of the thing's attributes or single Attr-Value
get_frame(Name, Slots) :-
    get_frame_slots(Name, Slots).
get_frame(Name, Required) :-
    \+ get_frame_slots(Name, Required),
    get_frame_slots(Name, Slots),
    get_from_slots(Required, Slots).

%! get_frame_slots(Name, Slots) is nondet.
%  a frame is either from the upper ontology, or user ontology.
%  It's most likely that the query will be for the user ontology,
%  so that is checked first to improve performance. Helper to
%  [[get_frame/2]]. Distinct to handle getting from slots compared
%  to query format Attr-Value possible in [[get_frame/2]].
%
%  @arg Name A unique identifier of a frame
%  @arg Slots A list of [Attr-Value]
get_frame_slots(Name, Slots) :-
    ( mframe(Name, Slots)
    ; frame(Name, Slots)
    ).

%! get_from_slots(?Required, +Slots:list) is nondet.
%  extract the required attr-value pairs from the provided slots
get_from_slots([], _).  % Nothing required
get_from_slots(Attr-Value, Slots) :-  % Singular required in Slots
    member(Attr-Value, Slots),
    \+ is_list(Value).
get_from_slots(Attr-Value, Slots) :-  % List of values, yield with member
    member(Attr-List, Slots),
    is_list(List),
    member(Value, List).
get_from_slots(Attr-Value, Slots) :-  % check ako
    member(ako-Ako, Slots),
    get_frame(Ako, Attr-Value).
get_from_slots(isa-Value, Slots) :-
    member(isa-Ako, Slots),
    get_frame(Ako, ako-Value).
get_from_slots(Attr-Value, Slots) :-  % check isa
    member(isa-Ako, Slots),
    get_frame(Ako, Attr-Value).
get_from_slots([Required|Tail], Slots) :-  % List of Req, recurse.
    get_from_slots(Required, Slots),
    get_from_slots(Tail, Slots).


%! add_frame(+Name, +NewData) is det.
%  Add data to an existing frame or make a new frame with that name.
%  This is "over-write" data, it will clobber existing attrs.
%
% @arg Name A unique identifier of a frame
% @arg NewData a list of, or single Attr-Value
% @tbd Consider over-write version
add_frame(Name, Attr-Val) :-
    add_frame(Name, [Attr-Val]), !.
add_frame(Name, NewData) :-
    with_mutex(frames, mframe(Name, Slots)),
    combine_slots(NewData, Slots, Combined),
    flatten(Combined, NewSlots),
    update_data(Name, Slots, NewSlots), !.
add_frame(Name, Slots) :-
    \+ with_mutex(frames, mframe(Name, _)),
    assert_mframe(Name, Slots), !.

combine_slots([], Combined, Combined).
combine_slots([Attr-Val|T], Slots, Combined) :-
    (member(Attr-_, Slots) ->
        clobber(Attr-Val, Slots, NewSlots)
    ; NewSlots = [Attr-Val|Slots]
    ),
    combine_slots(T, NewSlots, Combined).

clobber(Attr-Val, Slots, [Attr-Val|Removed]) :-
    select(Attr-_, Slots, Removed).



%! remove_frame(+Name, +RmData) is semidet.
%  Remove the provided data from a frame.
%
% @arg Name A unique identifier for a frame
% @arg RmData a list of, or single Attr-Value
remove_frame(Name, RmData) :-
    with_mutex(frames, mframe(Name, Slots)),
    member(RmData, Slots),
    select(RmData, Slots, NewSlots),
    update_data(Name, Slots, NewSlots), !.
remove_frame(Name, RmData) :-
    with_mutex(frames, mframe(Name, Slots)),
    is_list(RmData),
    subtract(Slots, RmData, NewSlots),
    update_data(Name, Slots, NewSlots), !.

%! remove_frame(+Name) is semidet.
%  Remove the named frame from the in-memory store.
%
% @arg Name A unique identifier for a frame
remove_frame(Name) :-
    retractall_mframe(Name, _),
    db_sync(gc(always)), !.


%! update_data(+Name, +OldSlots:list, +NewSlots:list) is det.
%  retract frame(Name, OldSlots) and assert frame(Name, NewSlots)
update_data(Name, OldSlots, NewSlots) :-
    retractall_mframe(Name, OldSlots),
    assert_mframe(Name, NewSlots),
    db_sync(gc(always)).
